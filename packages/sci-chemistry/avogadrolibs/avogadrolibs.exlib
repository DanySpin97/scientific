# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenChemistry ] cmake

export_exlib_phases src_prepare src_compile src_install

SUMMARY="A chemical editor and visualization application"
DESCRIPTION="
Avogadro libraries provide 3D rendering, visualization, analysis and data
processing useful in computational chemistry, molecular modeling,
bioinformatics, materials science, and related areas."

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    doc
    hdf5 [[ description = [ Read and write large data in an HDF5 file store ] ]]
    plugindownloader [[ description = [ Downloads plugins, molecule data, etc ] ]]
    qt5
    symmetry [[ description = [ Plugin allowing to work with molecular symmetry ] ]]
    vtk [[
        description = [ Visualize molecules with the help of VTK ]
    ]]

    ( plugindownloader symmetry vtk ) [[ *requires = qt5 ]]
"

DEPENDENCIES="
    build:
        dev-libs/json[>=3.3.0]
        sci-libs/eigen:3
        doc? ( app-doc/doxygen )
        qt5? (
            dev-lang/python:2.7
            x11-libs/qttools:5 [[ note = [ Qt5LinguistTools ] ]]
        )
    build+run:
        media-libs/glew
        sci-chemistry/spglib
        x11-dri/mesa
        hdf5? ( sci-libs/hdf5 )
        plugindownloader? ( app-arch/libarchive )
        qt5? (
            sci-libs/molequeue
            x11-libs/qtbase:5[gui]
        )
        symmetry? ( sci-chemistry/libmsym )
        vtk? (
            sci-libs/vtk
            x11-libs/qtbase:5
        )
    test:
        dev-cpp/gtest
    run:
        dev-lang/python:*
"

BUGS_TO="heirecka@exherbo.org"

# Test fails to build due to cmake and threads...
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    # If set to false, boost would be required
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    # mmtf-cpp is unpackaged and doesn't have any proper upstream release yet.
    -DUSE_MMTF:BOOL=FALSE
    -DUSE_SPGLIB:BOOL=TRUE
    # I have very litte interest in this, but feel free to add support for it.
    -DUSE_PYTHON:BOOL=FALSE
    # I assume this needs https://github.com/OpenChemistry/protocall - unwritten.
    # It would also pull in Qt4 for a single plugin.
    -DUSE_PROTOCALL:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc DOCUMENTATION' )
CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    'qt5 TRANSLATIONS'
)
CMAKE_SRC_CONFIGURE_OPTION_USES+=(
    HDF5
    'plugindownloader LIBARCHIVE'
    'qt5 MOLEQUEUE'
    'qt5 OPENGL'
    'qt5 QT'
    'symmetry LIBMSYM'
    VTK
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTING:BOOL=TRUE -DENABLE_TESTING:BOOL=FALSE'
)

avogadrolibs_src_prepare() {
    cmake_src_prepare

    # remove bundled dev-libs/json
    edo rm -rf thirdparty/nlohmann

    # TODO: fix upstream
    edo sed \
        -e 's:${INSTALL_DOC_DIR}/avogadrolibs:${INSTALL_DOC_DIR}:g' \
        -i CMakeLists.txt
}

avogadrolibs_src_compile() {
    default

    option doc && emake documentation
}

avogadrolibs_src_install() {
    cmake_src_install

    if option doc ; then
        pushd "${WORK}"/docs
        dodoc -r html
        popd
    fi
}

