# Copyright 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.gz ]

export_exlib_phases src_install

SUMMARY="GNU Linear Programming Kit"
DESCRIPTION="
The GLPK (GNU Linear Programming Kit) package is intended for solving
large-scale linear programming (LP), mixed integer programming (MIP), and other
related problems. It is a set of routines written in ANSI C and organized in
the form of a callable library.  GLPK supports the GNU MathProg modeling
language, which is a subset of the AMPL language.

The GLPK package includes the following main components:
* Primal and dual simplex methods
* Primal-dual interior-point method
* Branch-and-cut method
* Translator for GNU MathProg
* Application program interface (API)
* Stand-alone LP/MIP solver
"

LICENCES="
    GPL-3
    ZLIB [[ note = [ bundled version of zlib ] ]]
"
SLOT="0"
MYOPTIONS="
    mysql [[ description = [ Allows you to transmit data between MathProg model objects and MySQL relational databases ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/gmp:= [[ description = [ The exact simplex solver can use this, it's preferred over the GLPK bignum module, which is much slower ] ]]
        mysql? ( virtual/mysql )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dl     # Enable shared library support, needed for mysql, odbc.
    # Unwritten
    --disable-odbc  # MathProg ODBC support
    --disable-static
    --with-gmp
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( mysql )

glpk_src_install() {
    default

    dodoc $(find doc/ -type f -name '*.pdf')
    insinto /usr/share/doc/${PNVR}

    emake distclean -C examples
    doins -r examples/
}

