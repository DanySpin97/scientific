# Copyright 2007 Danny van Dyk
# Copyright 2009 Ingmar Vanhassel
# Copyright 2009, 2015 Thomas G. Anderson <tanderson@caltech.edu>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}-${PV/_p/-pl}

export_exlib_phases src_test_expensive src_prepare src_configure src_compile src_install

SUMMARY="FFTW is a C subroutine library for computing the discrete Fourier transform"
DESCRIPTION="
FFTW is a C subroutine library for computing the discrete Fourier transform
(DFT) in one or more dimensions, of arbitrary input size, and of both real and
complex data (as well as of even/odd data, i.e. the discrete cosine/sine
transforms or DCT/DST)
FFTW's performance is typically superior to that of other publicly available
FFT software, and is even competitive with vendor-tuned codes. In contrast to
vendor-tuned codes, however, FFTW's performance is portable: the same program
will perform well on most architectures without modification. Hence the name,
\"FFTW\", which stands for the somewhat whimsical title of \"Fastest Fourier
Transform in the West.\"
"
HOMEPAGE="http://www.fftw.org"
DOWNLOADS="${HOMEPAGE}/${MY_PNV}.tar.gz"

BUGS_TO="tanderson@caltech.edu"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/faq [[ lang = en description = FAQ ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/release-notes.html [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    fortran openmp mpi
    quadmath [[ description = [ Add support for quadruple-precision arithmetic libraries ] ]]
    (
        platform: amd64 x86
        amd64_cpu_features: avx avx2 avx512
        x86_cpu_features: avx avx2 avx512 sse2
    )
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        fortran? ( sys-libs/libgfortran:= )
        openmp? ( sys-libs/libgomp:= )
        mpi? ( sys-cluster/openmpi )
        quadmath? ( sys-libs/libquadmath:= )
"

WORK=${WORKBASE}/${MY_PNV}
ECONF_SOURCE="${WORK}"

changesrcdir() {
    edo pushd ${1}
    shift
    edo "${@}"
    edo popd
}

fftw_src_prepare() {
    default
    edo mkdir float normal quad
}

fftw_src_configure() {
    local myconf=(
        --enable-shared
        --enable-threads
        --disable-doc
        --disable-static
        $(option_enable fortran)
        $(option_enable openmp)
        $(option_enable mpi)
    )

    # NOTE(tanderson): Architecture-dependent instruction support is only
    # implemented for the single and double precision libraries, not for
    # quadruple precision.
    local my_singledouble_conf=()
    if option platform:amd64 ; then
        my_singledouble_conf+=(
            --enable-sse2
            $(option_enable amd64_cpu_features:avx)
            $(option_enable amd64_cpu_features:avx2)
            $(option_enable amd64_cpu_features:avx512)
        )
    elif option platform:x86 ; then
        my_singledouble_conf+=(
            $(option_enable x86_cpu_features:sse2)
            $(option_enable x86_cpu_features:avx)
            $(option_enable x86_cpu_features:avx2)
            $(option_enable x86_cpu_features:avx512)
        )
    fi

    changesrcdir float econf \
        --enable-float \
        "${myconf[@]}" \
        "${my_singledouble_conf[@]}"

    changesrcdir normal econf \
        "${myconf[@]}" \
        "${my_singledouble_conf[@]}"

    option quadmath && changesrcdir quad econf \
            --enable-quad-precision \
            "${myconf[@]}"

}

fftw_src_compile () {
    changesrcdir float default
    changesrcdir normal default
    option quadmath && changesrcdir quad default
}

fftw_src_test_expensive() {
    # fftw also provides the targets bigcheck, paranoid-check and
    # exhaustive-check
    changesrcdir float \
        emake check
    changesrcdir normal \
        emake check
    option quadmath && changesrcdir quad \
        emake check
}

fftw_src_install() {
    changesrcdir float default
    changesrcdir normal default

    option quadmath && changesrcdir quad default
    # The buildsystem always includes the quad precision fortran header,
    # regardless of whether we asked for it. Fix this.
    option quadmath || edo rm "${IMAGE}"/usr/$(exhost --target)/include/fftw3q.f03

    emagicdocs
}

