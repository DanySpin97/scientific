# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_HGREV="323c052e1731"

require cmake

export_exlib_phases src_test src_install

SUMMARY="A C++ template library for linear algebra (vectors, matrices, and related algorithms)"
DESCRIPTION="
Eigen is a C++ template library for linear algebra (vectors, matrices, and related algorithms). It is:
* Versatile. Eigen handles both fixed-size and dynamic-size matrices and vectors,
  without code duplication, and in a completely integrated way.
  It aims to provide not only vectors and matrices but also many algorithms.
* Fast. Expression templates remove temporaries and enable lazy evaluation.
  Explicit vectorization is performed for the SSE (2 and later) and AltiVec instruction sets,
  with graceful fallback to non-vectorized code. With fixed-size objects, dynamic memory allocation is avoided,
  and the loops are unrolled when that makes sense. For large matrices,
  special attention is paid to cache-friendliness.
* Good API. Expression templates allow for a very clean and expressive API.
  Implementing an algorithm on top of Eigen feels like just copying pseudocode.
"
HOMEPAGE="http://eigen.tuxfamily.org"
DOWNLOADS="https://bitbucket.org/${PN}/${PN}/get/${PV}.tar.bz2 -> ${PNV}.tar.bz2"

UPSTREAM_CHANGELOG="${HOMEPAGE}/index.php?title=ChangeLog#Eigen_${PV}"

LICENCES="LGPL-3 || ( GPL-2 GPL-3 )"
MYOPTIONS="
    sparse [[ description = [ Use SuiteSparse (Cholmod, UMFPACK) for sparse computations ] ]]
"

DEPENDENCIES="
    build:
        sparse? ( sci-libs/SuiteSparse )
    test:
        sci-libs/fftw
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_TESTING:BOOL=TRUE
    -DCMAKE_BUILD_TYPE:STRING=release
    # benchmarks have extra dependencies
    -DEIGEN_BUILD_BTL:BOOL=FALSE
    -DEIGEN_BUILD_PKGCONFIG:BOOL=TRUE
    -DEIGEN_SPLIT_LARGE_TESTS:BOOL=TRUE
    -DEIGEN_TEST_CXX11:BOOL=TRUE
    -DEIGEN_TEST_NOQT:BOOL=TRUE
    -DEIGEN_TEST_NO_OPENGL:BOOL=TRUE
    -DPKGCONFIG_INSTALL_DIR:PATH=/usr/$(exhost --target)/lib/pkgconfig
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

CMAKE_SOURCE=${WORKBASE}/${PN}-${PN}-${MY_HGREV}

eigen_src_test() {
    # "make test" won't build the test binaries, we have to use "make check"
    emake check
}

eigen_src_install() {
    cmake_src_install

    insinto /usr/share/cmake/Modules
    doins "${CMAKE_SOURCE}"/cmake/FindEigen3.cmake
}

