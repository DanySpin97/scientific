# Copyright 2013-2018 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SUMMARY="A mixed level/signal circuit simulator"
DESCRIPTION="
ngspice is a general-purpose circuit simulator program.
It implements three classes of analysis:
- Nonlinear DC analyses
- Nonlinear Transient analyses
- Linear AC analyses
"

LICENCES="LGPL-2.1 BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    openmp [[ description = [ Enable parallel processing using OpenMP ] ]]
    X [[ description = [ Build GUI tools ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison[>=2.7]
        sys-devel/flex
    build+run:
        sci-libs/fftw
        sys-libs/readline:=
        openmp? ( sys-libs/libgomp:= )
        X? (
            x11-libs/libX11
            x11-libs/libXaw
            x11-libs/libXext
            x11-libs/libXmu
            x11-libs/libXt
        )
"

# testsuite seems broken
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--hates=docdir'
    '--hates=datarootdir'

    '--disable-debug'
    '--disable-oldapps'
    '--disable-relpath'
    '--disable-rpath'
    '--with-readline'
    '--with-ngshared'
    '--without-editline'

    '--enable-capbypass'
    '--enable-cider'
    '--enable-fftw3'
    '--enable-nvdev'
    '--enable-predictor'
    '--enable-xspice'

    '--disable-adms'
    '--disable-checker'
    '--disable-cluster'
    '--disable-gprof'
    '--disable-pss'
    '--without-tcl'

    # deprecated
    '--disable-help'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'openmp'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X x'
)

