# Copyright 2010-2011 Denis Dupeyron <calchan@gentoo.org>
# Copyright 2013-2019 Marvin Schmidt <marv@exherbo.org>
# Copyright 2015 Jakub Kopański <jkopanski@quivade.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kicad-99999999' from Gentoo (same author), which is:
#     Copyright 1999-2010 Gentoo Foundation

require launchpad [ suffix=tar.xz branch=$(ever major).0 ]
require cmake [ api=2 ]
if ever at_least 5.1.2; then
    require wxwidgets [ abi=3.0 gtk_backends=[ 3 ] ]
else
    # GTK3 backend causes serious rendering issues in eeschema,
    require wxwidgets [ abi=3.0 gtk_backends=[ 2 ] ]
fi

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="KiCad GPL PCB suite"
DESCRIPTION="
KiCad is an open source (GPL) software for the creation of electronic schematic diagrams and printed
circuit board artwork.
"
HOMEPAGE="http://www.kicad-pcb.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build doxygen documentation ] ]]
    python [[ description = [ Python scripting support ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.54.0]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
        python? ( dev-lang/swig[python] )
    build+run:
        media-libs/glew
        net-misc/curl
        sys-libs/libgomp:=
        x11-dri/glu
        x11-dri/mesa
        x11-libs/cairo[>=1.8.1]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        python? ( dev-lang/python:2.7 )
    run:
        sys-libs/zlib
    recommendation:
        sci-electronics/kicad-footprints [[
            description = [ Component footprints ]
        ]]
    suggestion:
        sci-electronics/kicad-doc [[
            description = [ User documentation and tutorials ]
        ]]
        sci-electronics/kicad-i18n [[
            description = [ Internationalization files ]
        ]]
"

if ever at_least 5.0.2; then
    DEPENDENCIES+="
        recommendation:
            sci-electronics/kicad-symbols [[
                description = [ Schematic symbols library ]
            ]]
        suggestion:
            sci-electronics/kicad-packages3D [[
                description = [ 3D models library ]
            ]]
    "
else
    DEPENDENCIES+="
        recommendation:
            sci-electronics/kicad-library [[
                description = [ Schematic symbols library and 3D models ]
            ]]
    "
fi

BUGS_TO="<calchan@gentoo.org>"

if ever at_least 5.1.2; then
    MYOPTIONS+="
        demos [[ description = [ Install the demo and example projects ] ]]
        simulator [[ description = [ Build the Spice based schematic simulator ] ]]
    "
    DEPENDENCIES+="
        build:
            dev-libs/glm[>=0.9.5.1]
            python? ( dev-lang/swig[>=3.0][python] )
        build+run:
            x11-libs/cairo[>=1.12.0]
            x11-libs/pixman:1[>=0.30]
            simulator? (
                sci-electronics/ngspice[>=26-r2] [[ note = [ First version to provide shared library ] ]]
            )
    "
fi

kicad_src_prepare() {
    cmake_src_prepare

    # obey datadir directory location
    if ever at_least 5.0.2; then
        edo sed -e 's#DESTINATION share#DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}#' \
                -i CMakeLists.txt

    else
        edo sed -e 's#DESTINATION ${CMAKE_INSTALL_PREFIX}/share#DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}#' \
                -i CMakeLists.txt
    fi

    if ! ever at_least 5.0.2; then
        # No need for INSTALL.txt
        edo sed -e '/install( FILES INSTALL.txt/,/COMPONENT resources )/d' -i CMakeLists.txt
    fi
}

kicad_src_configure() {
    local myconf=()

    myconf+=(
        -DwxWidgets_CONFIG_EXECUTABLE=$(wxwidgets_get_config)

        -DDEFAULT_INSTALL_PATH:PATH=/usr
        -DKICAD_DATA:PATH=/usr/share/kicad

        # needs to be DEFAULT_INSTALL_PATH/share/doc/kicad/ in order for the help files
        # to be found (path is hardcoded)
        -DKICAD_DOCS:PATH=/usr/share/doc/${PN}

        -DBUILD_GITHUB_PLUGIN:BOOL=TRUE

        $(cmake_option python KICAD_SCRIPTING)
        $(cmake_option python KICAD_SCRIPTING_MODULES)
        # FIXME: Requires wxPython:3.0 built with gtk+:2 support
        # $(cmake_option python KICAD_SCRIPTING_WXPYTHON)
    )

    if ever at_least 5.1.2; then
        myconf+=(
            -DKICAD_USE_OCC:BOOL=OFF
            -DKICAD_USE_OCE:BOOL=OFF

            -DKICAD_SCRIPTING_PYTHON3:BOOL=OFF
            -DKICAD_SCRIPTING_WXPYTHON:BOOL=OFF
            -DKICAD_SCRIPTING_WXPYTHON_PHOENIX:BOOL=OFF

            $(cmake_option demos KICAD_INSTALL_DEMOS)
            $(cmake_option python KICAD_SCRIPTING_ACTION_MENU)
            $(cmake_option simulator KICAD_SPICE)
        )
    fi

    ecmake "${myconf[@]}"
}

kicad_src_compile() {
    default
    option doc && emake doxygen-docs
}

kicad_src_install() {
    cmake_src_install

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"
        edo mv Documentation/doxygen/html doxygen
        dodoc -r doxygen
        edo popd
    fi
}

